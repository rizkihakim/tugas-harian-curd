@extends('adminlte.master')

@section('content')
  <div class ="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Bordered Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                  <div class ="alert alert-success">
                    {{  session('success')  }}
                  </div>
                @endif
                <a href="/pertanyaan/create" class="btn btn-primary mb-2">Pertanyaan Baru</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Pertanyaan</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $baris)
                      <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $baris->isi }} </td>
                        <td style="display: flex;">
                          <a href="/pertanyaan/{{$baris->id}}" class="btn btn-info btn-sm">Show</a>
                          <a href="/pertanyaan/{{$baris->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                          <form action="/pertanyaan/{{$baris->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger btn-sm" value="delete">
                          </form>
                        </td>
                      </tr>
                    @empty
                      <tr>
                        <td colspan="4" align="center"> Tidak ada pertanyaan</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->

            </div>
  </div>
@endsection
