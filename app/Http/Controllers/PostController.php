<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create() {
      return view('pertanyaan.create');
    }

    public function store(Request $request) {
      //dd($request -> all());
      $request->validate([
        'isi' => 'required|unique:pertanyaan'
      ]);
      $query = DB::table('pertanyaan') -> insert([
        "isi" => $request["isi"]
      ]);

      return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function index() {
      $pertanyaan = DB::table('pertanyaan')->get();
      //dd($pertanyaan);
      return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($pertanyaanId) {
      $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaanId)->first();

      return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($pertanyaanId) {
      $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaanId)->first();

      return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($pertanyaanId, Request $request) {
      $request->validate([
        'isi' => 'required|unique:pertanyaan'
      ]);

      $query = DB::table('pertanyaan')
                  ->where('id', $pertanyaanId)
                  ->update([
                    'isi' => $request['isi']
                  ]);
      return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan!');
    }

    public function destroy($pertanyaanId, Request $request) {
      $query = DB::table('pertanyaan')->where('id', $pertanyaanId)->delete();

      return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
