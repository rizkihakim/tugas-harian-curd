<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/data-tables', function () {
    return view('items.data');
});

Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/pertanyaan/create', 'PostController@create');
Route::post('/pertanyaan', 'PostController@store');
Route::get('/pertanyaan', 'PostController@index');
Route::get('/pertanyaan/{pertanyaanId}', 'PostController@show');
Route::get('/pertanyaan/{pertanyaanId}/edit', 'PostController@edit');
Route::put('/pertanyaan/{pertanyaanId}', 'PostController@update');
Route::delete('/pertanyaan/{pertanyaanId}', 'PostController@destroy');
